export class DiscussSummary {
    id: string;
    questionText: string;
    likeCount: number;
}
