import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.scss'],
})
export class ProfileMenuComponent implements OnInit {
  public appPages = [
    {
      title: 'Library',
      url: '/folder/Inbox',
      icon: 'library',
    },
    {
      title: 'Your Creations',
      url: '/folder/Outbox',
      icon: 'paper-plane',
    },
    {
      title: 'Notifications',
      url: '/folder/Favorites',
      icon: 'notifications',
    },
    {
      title: 'Settings',
      url: '/folder/Archived',
      icon: 'settings',
    },
    {
      title: 'Report a Problem',
      url: '/folder/Trash',
      icon: 'warning',
    },
    {
      title: 'Log Out',
      url: '/folder/Spam',
      icon: 'log-out',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
