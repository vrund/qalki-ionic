import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('../home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'library',
        loadChildren: () =>
          import('../library/library.module').then((m) => m.LibraryPageModule),
      },
      {
        path: 'discuss',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../discuss/discuss.module').then(
                (m) => m.DiscussPageModule
              ),
          },
          {
            path: ':id',
            loadChildren: () =>
              import(
                '../discuss/detailed-discuss-post/detailed-discuss-post.module'
              ).then((m) => m.DetailedDiscussPostPageModule),
          },
        ],
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('../profile/profile.module').then((m) => m.ProfilePageModule),
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
