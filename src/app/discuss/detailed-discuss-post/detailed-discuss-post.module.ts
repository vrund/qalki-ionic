import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailedDiscussPostPageRoutingModule } from './detailed-discuss-post-routing.module';

import { DetailedDiscussPostPage } from './detailed-discuss-post.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailedDiscussPostPageRoutingModule
  ],
  declarations: [DetailedDiscussPostPage]
})
export class DetailedDiscussPostPageModule {}
