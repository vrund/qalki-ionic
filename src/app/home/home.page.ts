import { Component, OnInit } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Content } from '../models/content';
import { ModalController } from '@ionic/angular';
import { AddContentComponent } from '../components/add-content/add-content.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  private readonly collectionName = 'content';

  list: Content[];
  contentCollection: AngularFirestoreCollection<Content>;

  constructor(
    private afs: AngularFirestore,
    private modalCtrl: ModalController
  ) {
    this.contentCollection = this.afs.collection<Content>(this.collectionName);
    this.contentCollection.valueChanges().subscribe((data) => {
      this.list = data;
    });
  }

  ngOnInit() {
    // for (let i = 0; i < 50; i++) {
    //   this.contentCollection.add({
    //     id: i.toString(),
    //     title: 'Title of the content',
    //     content: `Keep close to Nature's heart... and break clear away, once in awhile,
    //   and climb a mountain or spend a week in the woods. Wash your spirit clean.`,
    //     createdAt: new Date().toString(),
    //     createdBy: 'vrundpatel',
    //   } as Content);
    // }
  }

  async addContent() {
    const modal = await this.modalCtrl.create({
      component: AddContentComponent,
    });

    modal.onDidDismiss().then((params) => {
      console.log('data passed from modal ', params.data.newContent);
      this.contentCollection.add(params.data.newContent as Content);
    });

    return await modal.present();
  }


}
