import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Content } from 'src/app/models/content';

@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.component.html',
  styleUrls: ['./add-content.component.scss'],
})
export class AddContentComponent implements OnInit {
  title: string;
  content: string;
  constructor(private modalCtrl: ModalController) {
  }

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss({
      newContent: {
        id: 'newPostId',
        title: this.title,
        content: this.content,
        createdAt: new Date().toString(),
        createdBy: 'vrundpatel',
      } as Content,
    });
  }
}
