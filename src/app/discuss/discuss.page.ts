import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AskQuestionComponent } from './ask-question/ask-question.component';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { DiscussSummary } from '../models/discuss-summary.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-discuss',
  templateUrl: './discuss.page.html',
  styleUrls: ['./discuss.page.scss'],
})
export class DiscussPage implements OnInit {
  private readonly collectionName = 'discuss';

  list: DiscussSummary[];
  summaryCollection: AngularFirestoreCollection<DiscussSummary>;

  constructor(
    public modalController: ModalController,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.summaryCollection = this.afs.collection<DiscussSummary>(this.collectionName);
    this.summaryCollection.valueChanges().subscribe((data) => {
      this.list = data;
    });
  }

  ngOnInit() {}

  goToQuestion() {
    // alert('question clicked');
    // navigate to the detailed page for answering
    this.router.navigateByUrl(`${this.router.url}/10`);
  }

  addQuestion() {
    // const modal = await this.modalController.create({
    //   component: AskQuestionComponent,
    // });
    // return await modal.present();

    this.summaryCollection.add({
      id: '1',
      questionText: 'Lorem ipsum dolor amet lfjalfdjiakfjdalfjaldfjalfjlajdflajflajf',
      likeCount: 0
    } as DiscussSummary);
  }


}
