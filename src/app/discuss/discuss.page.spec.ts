import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiscussPage } from './discuss.page';

describe('DiscussPage', () => {
  let component: DiscussPage;
  let fixture: ComponentFixture<DiscussPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscussPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiscussPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
