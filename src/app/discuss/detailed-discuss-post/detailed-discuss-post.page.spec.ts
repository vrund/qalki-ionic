import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailedDiscussPostPage } from './detailed-discuss-post.page';

describe('DetailedDiscussPostPage', () => {
  let component: DetailedDiscussPostPage;
  let fixture: ComponentFixture<DetailedDiscussPostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedDiscussPostPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailedDiscussPostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
