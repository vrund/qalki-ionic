import { Timestamp } from 'rxjs';

export class Content {
    id: string;
    title: string;
    content: string;
    createdAt: string;
    createdBy: string;
}
