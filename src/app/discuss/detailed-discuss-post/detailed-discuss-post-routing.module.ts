import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailedDiscussPostPage } from './detailed-discuss-post.page';

const routes: Routes = [
  {
    path: '',
    component: DetailedDiscussPostPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailedDiscussPostPageRoutingModule {}
